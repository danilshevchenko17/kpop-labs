enum Category {
    Business_analyst,
    Developer,
    Designer,
    QA,
    Scrum_master
};
function getAllworkers() {
    var workers = [
        { Name: 'Ivan', surname: 'Ivanov', available: true, salary: 1000, Category: Category.Developer, id : 1},
        { Name: 'Petro', surname: 'Petrov', available: true, salary: 1500, Category: Category.Developer, id : 2},
        { Name: 'Vasyl', surname: 'Vasyliev', available: false, salary: 1600, Category: Category.Designer, id : 3},
        { Name: 'Evgen', surname: 'Zhukov', available: true, salary: 1300, Category: Category.QA, id : 4}
    ];
    return workers;
}

function logFirstAvailable(workers: any[] = getAllworkers()): void {
    var count = 0;   
    console.log("Avalible workers :");
    workers.forEach(worker => {
        if(worker.available == true) {
            console.log(worker.Name);
            count++;
        }
    });
    console.log("Ammount workers avalible: " + count);
    for(var item of workers) {
        console.log(item);
    }
}
console.log("Task1");
logFirstAvailable();

function getWorkersNamesByCategory(c:Category = Category.Designer) {
    let array = new Array()
    let workers = getAllworkers();
    workers.forEach(worker => {
        if(worker.Category == c) {
            array.push(worker.Name);
        }
    });
    return array;
}

function logWorkersNames(array: Array<string>) {
    for (let name of array) {
        console.log(name);
    }
}
console.log("Task2")
logWorkersNames(getWorkersNamesByCategory(Category.Developer));
console.log("Task3")
getAllworkers().forEach(worker => { 
    if(worker.Category == Category.Developer)
        console.log(`Name: ${worker.Name}, Surname: ${worker.surname}`);
});

function getWorkerByID(id: number) {
    let worker = getAllworkers().find(worker => worker.id == id );
    if(worker)
        return worker;
    return [];
}
console.log(getWorkerByID(2));

function createCustomerID(name: string, id: number): string {
    return id + " " + name;
}
console.log("Task4")
console.log(createCustomerID("Ann", 10));

let IdGenerator = (name: string, id: number): string => { return id + " " + name; }

console.log(IdGenerator("Ann", 1))
IdGenerator = createCustomerID;
console.log(IdGenerator("Ann", 2))

console.log("Task5")
function createCustomer(name: string, age?: number, city?: string): void {
    let result: string = name
    if (age)  result += ", " + age;
    if (city) result += ", " + city;
    console.log(result);
} 

createCustomer("Ann");
createCustomer("Ann", 3);
createCustomer("Ann", 4, "London ");

console.log(getWorkersNamesByCategory());
logFirstAvailable()

function checkoutWorkers(customer: string, ...workerIds: number[]): string[] {
    console.log(`Customer: ${customer}`);
    let availableWorkers: string[] = [];
    for (let id of workerIds) {
        let worker: any = getWorkerByID(id);
        if (worker.available) {
            availableWorkers.push(`${worker.Name} ${worker.surname}`);
        }
    }
    return availableWorkers;
}


let myWorkers = checkoutWorkers('Ann', 1, 2, 4);

myWorkers.forEach(element => {
    console.log(element);
});

