"use strict";
var Category;
(function (Category) {
    Category[Category["Business_analyst"] = 0] = "Business_analyst";
    Category[Category["Developer"] = 1] = "Developer";
    Category[Category["Designer"] = 2] = "Designer";
    Category[Category["QA"] = 3] = "QA";
    Category[Category["Scrum_master"] = 4] = "Scrum_master";
})(Category || (Category = {}));
;
function getAllworkers() {
    var workers = [
        { Name: 'Ivan', surname: 'Ivanov', available: true, salary: 1000, Category: Category.Developer, id: 1 },
        { Name: 'Petro', surname: 'Petrov', available: true, salary: 1500, Category: Category.Developer, id: 2 },
        { Name: 'Vasyl', surname: 'Vasyliev', available: false, salary: 1600, Category: Category.Designer, id: 3 },
        { Name: 'Evgen', surname: 'Zhukov', available: true, salary: 1300, Category: Category.QA, id: 4 }
    ];
    return workers;
}
function logFirstAvailable(workers) {
    if (workers === void 0) { workers = getAllworkers(); }
    var count = 0;
    console.log("Avalible workers :");
    workers.forEach(function (worker) {
        if (worker.available == true) {
            console.log(worker.Name);
            count++;
        }
    });
    console.log("Ammount workers avalible: " + count);
    for (var _i = 0, workers_1 = workers; _i < workers_1.length; _i++) {
        var item = workers_1[_i];
        console.log(item);
    }
}
console.log("Task1");
logFirstAvailable();
function getWorkersNamesByCategory(c) {
    if (c === void 0) { c = Category.Designer; }
    var array = new Array();
    var workers = getAllworkers();
    workers.forEach(function (worker) {
        if (worker.Category == c) {
            array.push(worker.Name);
        }
    });
    return array;
}
function logWorkersNames(array) {
    for (var _i = 0, array_1 = array; _i < array_1.length; _i++) {
        var name_1 = array_1[_i];
        console.log(name_1);
    }
}
console.log("Task2");
logWorkersNames(getWorkersNamesByCategory(Category.Developer));
console.log("Task3");
getAllworkers().forEach(function (worker) {
    if (worker.Category == Category.Developer)
        console.log("Name: " + worker.Name + ", Surname: " + worker.surname);
});
function getWorkerByID(id) {
    var worker = getAllworkers().find(function (worker) { return worker.id == id; });
    if (worker)
        return worker;
    return [];
}
console.log(getWorkerByID(2));
function createCustomerID(name, id) {
    return id + " " + name;
}
console.log("Task4");
console.log(createCustomerID("Ann", 10));
var IdGenerator = function (name, id) { return id + " " + name; };
console.log(IdGenerator("Ann", 1));
IdGenerator = createCustomerID;
console.log(IdGenerator("Ann", 2));
console.log("Task5");
function createCustomer(name, age, city) {
    var result = name;
    if (age)
        result += ", " + age;
    if (city)
        result += ", " + city;
    console.log(result);
}
createCustomer("Ann");
createCustomer("Ann", 3);
createCustomer("Ann", 4, "London ");
console.log(getWorkersNamesByCategory());
logFirstAvailable();
function checkoutWorkers(customer) {
    var workerIds = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        workerIds[_i - 1] = arguments[_i];
    }
    console.log("Customer: " + customer);
    var availableWorkers = [];
    for (var _a = 0, workerIds_1 = workerIds; _a < workerIds_1.length; _a++) {
        var id = workerIds_1[_a];
        var worker = getWorkerByID(id);
        if (worker.available) {
            availableWorkers.push(worker.Name + " " + worker.surname);
        }
    }
    return availableWorkers;
}
var myWorkers = checkoutWorkers('Ann', 1, 2, 4);
myWorkers.forEach(function (element) {
    console.log(element);
});
