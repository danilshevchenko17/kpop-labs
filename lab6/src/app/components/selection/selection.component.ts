import { Component, Output, EventEmitter } from '@angular/core'
import { WeatherDataService } from '../../services/weather-data.service'
import { Weather } from '../../app.component'

@Component({
  selector: 'app-selection',
  templateUrl: './selection.component.html',
  styleUrls: ['./selection.component.css']
})
export class SelectionComponent {
  @Output() onSelection: EventEmitter<Weather> = new EventEmitter<Weather>()
  city: String = ""

  constructor(private weatherData: WeatherDataService) { }

  submit() {
    const weather: Weather = new Weather()

    this.weatherData.load(this.city).subscribe(data => {
      weather.city = data['name']
      weather.conditions = data['weather'][0]['main']
      weather.temperature = (Math.round((data['main']['temp'] - 273.15)*1.8 + 32)-32)*5/9
      weather.icon = this.weatherData.getIconUrl(data['weather'][0]['icon'])

      this.onSelection.emit(weather)
      this.city = '';
    })
  }
}