//2.7
abstract class ReferenceItem {
    public title:string;
    protected year:number

    constructor(public newTitle: string, private newYear: number) {
        this.title = newTitle
        this.year = newYear
        console.log('Creating a new ReferenceItem ...')
    }

    public printItem() {
        console.log(`${this.title} was published in ${this.year}, department : ${ReferenceItem.department}`);
    }

    private _publisher: string;

    get publisher(): string {
        return this._publisher.toUpperCase();
    }

    set publisher(str) {
        this._publisher = str;
    }
    
    public static department: string = 'default';

    public abstract printCitation() : void;
}

class Encyclopedia extends ReferenceItem {
    public edition: number
    constructor(title: string, year: number, n_edition: number) {
        super(title, year);
        this.edition = n_edition
    }

    public printItem(): void {
        super.printItem();
        console.log("Edition: " + this.edition + " " + this.year);
    }

    public printCitation() {
        console.log(this.title + " - " + this.year);
    }
}

var EncyclopediaVar = new Encyclopedia('EncyclopediaName', 1990, 4)
EncyclopediaVar.printItem()
EncyclopediaVar.printCitation()