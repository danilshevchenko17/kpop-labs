//2.5
class ReferenceItem {
    public title:string;
    public year:number

    constructor(public newTitle: string, private newYear: number) {
        this.title = newTitle
        this.year = newYear
        console.log('Creating a new ReferenceItem ...')
    }

    public printItem() {
        console.log(this.title + " was published in " + this.year + ", department : " + ReferenceItem.department);
    }

    private _publisher: string;

    get publisher(): string {
        return this._publisher.toUpperCase();
    }

    set publisher(str) {
        this._publisher = str;
    }
    
    public static department: string = 'default';
}

var ref = new ReferenceItem('Demons', 1872)
ref.printItem()
ref.publisher = 'Azbuka'
