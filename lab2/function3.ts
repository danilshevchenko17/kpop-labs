//2.3
interface Person {
    name: string,
    email: string
}

interface Author extends Person {
    numBooksPublished: number;
}

interface Librarian extends Person {
    department: string,
    assistCustomer: (custName: string) => void
}

var favoriteAuthor : Author = {
    name : 'Fyodor Dostoevsky',
    email : 'FyodorDostoevsky@email.com',
    numBooksPublished : 20
}

var favoriteLibrarian : Librarian = {
    name : 'Name Surname',
    email : 'NameSurname@email.com',
    department : 'department',
    assistCustomer: (custName: string) : void => {
        console.log("helped " + custName + " choose a book")
    }
};
favoriteLibrarian.assistCustomer("Vasiliy")