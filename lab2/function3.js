var favoriteAuthor = {
    name: 'Fyodor Dostoevsky',
    email: 'FyodorDostoevsky@email.com',
    numBooksPublished: 10
};
var favoriteLibrarian = {
    name: 'Name Surname',
    email: 'NameSurname@email.com',
    department: 'department',
    assistCustomer: function (custName) {
        console.log("helped " + custName + " choose a book");
    }
};
favoriteLibrarian.assistCustomer("Vasiliy");
