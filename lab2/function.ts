//2.1
enum Category {
    Business_analyst,
    Developer,
    Designer,
    QA,
    Scrum_master
};

interface Worker_ {
    id:number,
    Name:string,
    surname:string,
    available:boolean,
    salary:number,
    Category:Category
}

function getAllworkers(): Worker_[] {
    return [
        { id: 1, Name: 'Ivan', surname: 'Ivanov', available: true, salary: 1000, Category: Category.Developer},
        { id: 2, Name: 'Petro', surname: 'Petrov', available: true, salary: 1500, Category: Category.Developer},
        { id: 3, Name: 'Vasyl', surname: 'Vasyliev', available: false, salary: 1600, Category: Category.Designer},
        { id: 4, Name: 'Evgen', surname: 'Zhukov', available: true, salary: 1300, Category: Category.QA},
    ];
}

function getWorkerByID(id: number): Worker_ | undefined {
    let worker = getAllworkers().find(worker => worker.id == id );
    if(worker)
        return worker;
    return undefined;
}

function PrintWorker(worker: Worker_) {
    console.log(worker.Name + " " + worker.surname + " got salary " + worker.salary);
}

PrintWorker(getWorkerByID(1));