var UniversityLibrarian = /** @class */ (function () {
    function UniversityLibrarian() {
    }
    UniversityLibrarian.prototype.assistCustomer = function (custName) {
        console.log(this.name + " is assisting " + custName);
    };
    return UniversityLibrarian;
}());
var favoriteAuthor = {
    name: 'Fyodor Dostoevsky',
    email: 'FyodorDostoevsky@email.com',
    numBooksPublished: 10
};
var favoriteLibrarian = new UniversityLibrarian();
favoriteLibrarian.name = 'Vasiliy';
favoriteLibrarian.assistCustomer('Danylo');
