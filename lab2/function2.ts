//2.2
type PrizeLogger = (arg0: string) => void

enum Category {
    Business_analyst,
    Developer,
    Designer,
    QA,
    Scrum_master
};

interface Worker__ {
    id:number,
    Name:string,
    surname:string,
    available:boolean,
    salary:number,
    Category:Category,
    markPrize:PrizeLogger
}

var logPrize: PrizeLogger = (prize) => console.log(prize);
logPrize("logPrize test call")