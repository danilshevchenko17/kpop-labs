//2.4
interface Person {
    name: string,
    email: string
}

interface Author extends Person {
    numBooksPublished: number;
}

interface Librarian extends Person {
    department: string,
    assistCustomer: (custName: string) => void
}

class UniversityLibrarian implements Librarian {
    public department: string;
    public email: string;
    public name: string;

    public assistCustomer(custName: string) : void {
        console.log(this.name + " is assisting " + custName)
    }
}

var favoriteLibrarian = new UniversityLibrarian();
favoriteLibrarian.name = 'Vasiliy';
favoriteLibrarian.assistCustomer('Danylo');