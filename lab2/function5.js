//2.5
var ReferenceItem = /** @class */ (function () {
    function ReferenceItem(newTitle, newYear) {
        this.newTitle = newTitle;
        this.newYear = newYear;
        this.title = newTitle;
        this.year = newYear;
        console.log('Creating a new ReferenceItem ...');
    }
    ReferenceItem.prototype.printItem = function () {
        console.log(this.title + " was published in " + this.year + ", department : " + ReferenceItem.department);
    };
    Object.defineProperty(ReferenceItem.prototype, "publisher", {
        get: function () {
            return this._publisher.toUpperCase();
        },
        set: function (str) {
            this._publisher = str;
        },
        enumerable: false,
        configurable: true
    });
    ReferenceItem.department = 'default';
    return ReferenceItem;
}());
var ref = new ReferenceItem('Demons', 1872);
ref.printItem();
ref.publisher = 'Azbuka';
