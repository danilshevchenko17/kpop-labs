var Category;
(function (Category) {
    Category[Category["Business_analyst"] = 0] = "Business_analyst";
    Category[Category["Developer"] = 1] = "Developer";
    Category[Category["Designer"] = 2] = "Designer";
    Category[Category["QA"] = 3] = "QA";
    Category[Category["Scrum_master"] = 4] = "Scrum_master";
})(Category || (Category = {}));
;
//2.1
function getAllworkers() {
    return [
        { id: 1, Name: 'Ivan', surname: 'Ivanov', available: true, salary: 1000, Category: Category.Developer },
        { id: 2, Name: 'Petro', surname: 'Petrov', available: true, salary: 1500, Category: Category.Developer },
        { id: 3, Name: 'Vasyl', surname: 'Vasyliev', available: false, salary: 1600, Category: Category.Designer },
        { id: 4, Name: 'Evgen', surname: 'Zhukov', available: true, salary: 1300, Category: Category.QA },
    ];
}
function getWorkerByID(id) {
    var worker = getAllworkers().find(function (worker) { return worker.id == id; });
    if (worker)
        return worker;
    return undefined;
}
function PrintWorker(worker) {
    console.log(worker.Name + " " + worker.surname + " got salary " + worker.salary);
}
PrintWorker(getWorkerByID(1));
