import { Component } from '@angular/core';
     
@Component({
    selector: 'my-app',
    template: `<label>Введіть радіус:</label>
    <input [(ngModel)]="radius" placeholder="your radius">
    <p>Радіус: {{radius}}</p>
    <p>Довжина окружності кола: {{length}}</p>
    <p>Площа кола: {{square}}</p>
    <p>Об'єм кулі: {{volume}}</p>
    <button (click)="countLenght()">Знайти довжину окружності кола:</button>
    <button (click)="countSquare()">Знайти площу круга:</button>
    <button (click)="countVolume()">Знайти об'єм кулі:</button>`


})
export class AppComponent { 
    radius : number = 0;
    length : number = 0;
    square : number = 0;
    volume : number = 0;
    countLenght(){
        this.length = 2*this.radius*Math.PI;
    }
    countSquare(){
        this.square = this.radius*this.radius*Math.PI;
    }
    countVolume(){
        this.volume = 4/3*Math.PI*Math.pow(this.radius,3);
    }
}
