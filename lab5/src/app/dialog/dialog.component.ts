import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';

interface DialogData {
  body: string;
  title: string;
}

interface adapter {
  ngOnInit(): void;
  closeDialog() : void;
}

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})

export class DialogComponent implements OnInit, adapter{

  private static _instance: DialogComponent;

  public static get Instance()
  {
      return this._instance
  }

  constructor(
    public dialogRef: MatDialogRef<DialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData ,
  ) {}

  public ngOnInit(): void {}

  public closeDialog() {
    this.dialogRef.close();
  }
}
